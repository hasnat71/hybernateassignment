package com.springHybernate.service;

import com.springHybernate.entity.Player;

import java.util.List;

public interface PlayerService {

    public List<Player> getPlayers();


    public Player getPlayerById(int id);

    public void SavePlayer(Player player);



    public  void  DeletePlayer(int id);


    void updatePlayer(Player player);
}
