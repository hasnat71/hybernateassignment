package com.springHybernate.service;

import com.springHybernate.entity.Country;

import java.util.List;

public interface CountryService {

    public List<Country> getCountries();

    public  void saveCountry(Country country);

    public  Country  getCountryById(int id);

    public  void  deleteCountry(int id );

}
