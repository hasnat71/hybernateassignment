package com.springHybernate.service;

import com.springHybernate.entity.Team;

import java.util.List;

public interface TeamService {
    public List<Team> getTeams();

    public  void saveTeam(Team team);

    public  Team  getTeamById(int id);

    public  void  deleteTeam(int id );

    public  void updateTeam(Team team);

    public void addPlayerToTeam(int playerId , int teamId);



}
